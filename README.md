## Synopsis

This module goal is to provide three new metatags that have recently been created by google to be compliant with the new european rules regarding copyright (mainly on news websites).

It adds the tag max-snippet, max-video-preview and max-image-preview.
More information on https://webmaster-fr.googleblog.com/2019/09/metatags-in-search.html (in French).

## Requirements

The module require the module Metatag to work correctly as it plug itself in the configuration page of Metatag.
